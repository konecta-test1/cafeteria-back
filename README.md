# Cafeteria Back

## Requirements

- PHP ^8.0.2
- Laravel ^9.11


## Installation

Clone repository

```
git clone https://gitlab.com/konecta-test1/cafeteria-back.git
```

In the folder, you have copy the `.env.example` file and paste it. Then, you have to rename the copy `.env` <br>

Example on Windows (Check this operation according to your OS).

```
copy .env.example .env
```

Install dependencies:
```
composer install
```

Generate an API KEY running this command:
```
php artisan key:generate
```

Generate The API Documentation running this command:
```
php artisan scribe:generate
```

You have to create a database called "cafeteria_back". After, run migrations with seeders:
```
php artisan migrate --seed
```

Install Passport keys, please run:
```
php artisan passport:install
```
 
You have to run 2 instances of application with two different Command-Line Interfaces this way:
```
php artisan serve
php artisan serve --port=8001
```

Now, you can make requests to the API to [127.0.0.1:8000](http://127.0.0.1:8000) and you see the documentation on [127.0.0.1:8000/docs](http://127.0.0.1:8000/docs) as well.

## information of available users
- Rol:Administrador <br>
username : pepito@konecta.com <br>
password : Ko12345678*
- Rol:Vendedor <br>
username : fulano@konecta.com <br>
password : Ko12345678*
- Rol:cliente <br>
username : sutano@konecta.com <br>
password : Ko12345678*

# Notice
Do not forget setting the SMTP variables in the `.env` file and running `php artisan queue:work` command.
