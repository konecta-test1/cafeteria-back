<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "pepito",
            "email" => "pepito@konecta.com",
            "password" => "Ko12345678*"
        ])->assignRole(1);

        User::create([
            "name" => "fulano",
            "email" => "fulano@konecta.com",
            "password" => "Ko12345678*"
        ])->assignRole(2);

        User::create([
            "name" => "sutano",
            "email" => "sutano@konecta.com",
            "password" => "Ko12345678*"
        ])->assignRole(3);
    }
}
