<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['id' => 1,'name' => 'administrador']);
        Role::create(['id' => 2,'name' => 'vendedor']);
        Role::create(['id' => 3,'name' => 'cliente']);

        Permission::create(['name'=>'access.panel'])->syncRoles(1,2);
        Permission::create(['name'=>'users.*'])->syncRoles(1);
        Permission::create(['name'=>'roles.*'])->syncRoles(1);
        Permission::create(['name'=>'products.*'])->syncRoles(1,2);
        Permission::create(['name'=>'products.read'])->syncRoles(3);
        Permission::create(['name'=>'sales.*'])->syncRoles(1,2);
        Permission::create(['name'=>'sales.create'])->syncRoles(3);
    }
}
