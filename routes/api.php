<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(function () {
    Route::post('logout', [AuthController::class,'logout'])->middleware('auth:api');
    Route::post('login',[AuthController::class,'login']);
    Route::post('refresh',[AuthController::class,'refresh']);
});
Route::middleware(['auth:api'])->group(function () {
    Route::apiResource('users',UserController::class);
    Route::apiResource('products',ProductController::class)->except('index');
    Route::apiResource('sales',SaleController::class)->except(['update','destroy']);
    Route::apiResource('roles',RoleController::class);
    Route::apiResource('permissions',PermissionController::class);
});
Route::get('products',[ProductController::class,'index']);
