<?php

namespace App\Traits;

trait BaseResponse {

    /**
     * Send a success http response.
     * 
     * @param int code
     * @param array|string data
     */
    public function successResponse($data = [],$code = 200)
    {
        return response()->json(['status'=>true,'data'=>$data],$code);
    }

    /**
     * Send a error http response.
     * 
     * @param int code
     * @param array|string data
     */
    public function errorResponse($data,$code = 400)
    {
        return response()->json(['status'=>false,'error'=>$data],$code);
    }
}