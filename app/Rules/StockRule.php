<?php

namespace App\Rules;

use App\Models\Product;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Log;

class StockRule implements Rule
{
    public $productWithError;
    public $products;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($products)
    {
        $this->products = collect($products);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $productTotalQuantity = $this->products->where('product_id',$value)->sum('quantity');
        $product = Product::find($value);
        if($productTotalQuantity > $product->stock){
            $this->productWithError = $product;
            return false;
        } 
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'There are not sufficient stock for product with id '.$this->productWithError->id;
    }
}
