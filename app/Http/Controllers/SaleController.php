<?php

namespace App\Http\Controllers;

use App\Events\NewSale;
use App\Http\Requests\Sale\CreateSaleRequest;
use App\Http\Resources\SaleResource;
use App\Models\Product;
use App\Models\Sale;
use App\Traits\BaseResponse;
use Illuminate\Http\Request;

/**
 * @group Sale
 */
class SaleController extends Controller
{
    use BaseResponse;

    public function __construct()
    {
        $this->middleware(['can:sales.read'])->only(['index','show']);
        $this->middleware(['can:sales.create'])->only(['store']);
    }

    /**
     * Display a listing of the sales.
     *
     * @apiResourceCollection App\Http\Resources\SaleResource
     * @apiResourceModel App\Models\Sale
     * @responseFile 401 /responses/auth/unauthenticated.json
     */
    public function index()
    {
        $sales = Sale::all();
        return $this->successResponse(SaleResource::collection($sales));
    }

    /**
     * Store a newly created sale in storage.
     *
     * @apiResource App\Http\Resources\SaleResource
     * @apiResourceModel App\Models\Sale
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 422 scenario="unprocessable content" /responses/sale/form_validation_fails.json
     */
    public function store(CreateSaleRequest $request)
    {
        $sale = Sale::create($request->only(['user_id']));
        $groupProducts = $this->groupProductsByIdAndSumQuantity(collect($request->products));
        foreach ($groupProducts as $item) {
            $product = Product::find($item['product_id']);
            $sale->products()->attach($product->id,['quantity'=>$item['quantity'],'total_price'=>($item['quantity'] * $product->price)]); 
        }
        NewSale::dispatch($sale);
        return $this->successResponse(new SaleResource($sale->load('products')));
    }

    /**
     * Display the specified sale.
     *
     * @apiResource App\Http\Resources\SaleResource
     * @apiResourceModel App\Models\Sale
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     */
    public function show(Sale $sale)
    {
        return $this->successResponse(new SaleResource($sale->load('products')));
    }

    function groupProductsByIdAndSumQuantity($collection)
{
    $groupProducts = $collection->groupBy('product_id');
    $groupProductsSum = $groupProducts->mapWithKeys(function ($group, $key) {
        return [
            "product_".$key =>
                [
                    'product_id' => $key, // $key is what we grouped by, it'll be constant by each  group of rows
                    'quantity' => $group->sum('quantity')
                ]
        ];
    });
    return $groupProductsSum;
}
}
