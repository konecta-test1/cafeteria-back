<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Traits\BaseResponse;
use Illuminate\Http\Request;

/**
 * @group Product
 */
class ProductController extends Controller
{
    use BaseResponse;

    public function __construct()
    {
        $this->middleware(['can:products.create'])->only(['store']);
        $this->middleware(['can:products.update'])->only(['update']);
        $this->middleware(['can:products.delete'])->only(['destroy']);
    }

    /**
     * Display a listing of the products.
     *
     * @apiResourceCollection App\Http\Resources\ProductResource
     * @apiResourceModel App\Models\Product
     * @responseFile 401 /responses/auth/unauthenticated.json
     * 
     */
    public function index()
    {
        $products = Product::all();
        return $this->successResponse(ProductResource::collection($products));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @apiResource App\Http\Resources\ProductResource
     * @apiResourceModel App\Models\Product
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 422 scenario="unprocessable content" /responses/product/form_validation_fails.json
     */
    public function store(CreateProductRequest $request)
    {
        $product = Product::create($request->all());
        return $this->successResponse(new ProductResource($product));
    }

    /**
     * Display the specified product.
     * 
     * @apiResource App\Http\Resources\ProductResource
     * @apiResourceModel App\Models\Product
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     */
    public function show(Product $product)
    {
        return $this->successResponse(new ProductResource($product));
    }

    /**
     * Update the specified product in storage.
     *
     * @apiResource App\Http\Resources\ProductResource
     * @apiResourceModel App\Models\Product
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     * @responseFile 422 scenario="unprocessable content" /responses/product/form_validation_fails.json
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->update($request->all());
        return $this->successResponse(new ProductResource($product->refresh()));
    }

    /**
     * Remove the specified product from storage.
     *
     * @apiResource App\Http\Resources\ProductResource
     * @apiResourceModel App\Models\Product
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return $this->successResponse(new ProductResource($product));
    }
}
