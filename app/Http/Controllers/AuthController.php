<?php

namespace App\Http\Controllers;

use App\Exceptions\AuthServiceUnavialable;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RefreshTokenRequest;
use App\Models\ClientPassport;
use App\Models\User;
use App\Traits\BaseResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Client as OClient;

/**
 * @group Authentication
 */
class AuthController extends Controller
{
    use BaseResponse;

    /**
     * This endpoint lets you get login
     * 
     * @unauthenticated
     * @responseFile status=200 /responses/auth/login_success.json
     * @responseFile status=401 scenario="incorrect credentials" /responses/auth/incorrect_credentials.json
     * @responseFile status=503 scenario="auth service unavaliable" /responses/auth/auth_service_unavailable.json
     */
    public function login(LoginRequest $request)
    {
        $oClient = OClient::where('password_client', 1)->first();
        try {
            $response = Http::asForm()->post(config('app.auth_url').'/oauth/token', [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $request->username,
                'password' => $request->password,
                'scope' => '',
            ]);
        } catch (\Throwable $th) {
            throw new AuthServiceUnavialable();
        }
        if (isset($response->json()['error'])) return $this->errorResponse($response->json(),422);

        $token = $response->json();
        $user = User::where('email',$request->username)->with(['roles'])->first();
        $user['all_permissions'] = $user->getAllPermissions();
        $token['user'] = $user;
        return $this->successResponse($token);
    }

    /**
     * This endpoint lets you refresh a token
     * 
     * @unauthenticated
     * @responseFile status=200 /responses/auth/login_success.json
     * @responseFile status=401 scenario="incorrect credentials" /responses/auth/incorrect_credentials.json
     * @responseFile status=503 scenario="auth service unavaliable" /responses/auth/auth_service_unavailable.json
     */
    public function refresh(RefreshTokenRequest $request)
    {
        $oClient = OClient::where('password_client', 1)->first();
        try {
            $response = Http::asForm()->post(config('app.auth_url').'/oauth/token', [
                'grant_type' => 'refresh_token',
                'refresh_token' => $request->refresh_token,
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'scope' => '',
            ]);
        } catch (\Throwable $th) {
            throw new AuthServiceUnavialable();
        }
        if (isset($response->json()['error'])) return $this->errorResponse($response->json(),401);
        return $this->successResponse($response->json());
    }

    /**
     * This endpoint lets you logout revoking the token
     * 
     * @responseFile status=200 /responses/auth/logout_success.json
     * @responseFile status=401 scenario="token invalid" /responses/auth/unauthenticated.json
     * 
     */
    public function logout()
    {
        $user = Auth::user()->token();
        $user->revoke();

        return $this->successResponse("success logout");
    }
}
