<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * @group User
 */
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['can:users.read'])->only(['index','show']);
        $this->middleware(['can:users.create'])->only(['store']);
        $this->middleware(['can:users.update'])->only(['update']);
        $this->middleware(['can:users.delete'])->only(['destry']);
    }

    /**
     * Display a listing of the users.
     *
     * @apiResourceCollection App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User
     * @responseFile 401 /responses/auth/unauthenticated.json
     */
    public function index()
    {
        $users = User::all();
        return UserResource::collection($users->load('roles'));
    }

    /**
     * Store a newly created user in storage.
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 422 scenario="unprocessable content" /responses/user/form_validation_fails.json
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->all();
        $data['password'] = '12345678';
        $user = User::create($data)->syncRoles($request->roles);
        return new UserResource($user);
    }

    /**
     * Display the specified user.
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     */
    public function show(User $user)
    {
        return new UserResource($user->load('roles'));
    }

    /**
     * Update the specified user in storage.
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     * @responseFile 422 scenario="unprocessable content" /responses/user/form_validation_fails.json
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->fill($request->except(['password']));
        $user->save();
        if (isset($request->roles)) $user->syncRoles($request->roles);
        return new UserResource($user->refresh());
    }

    /**
     * Remove the specified user from storage.
     *
     * @apiResource App\Http\Resources\UserResource
     * @apiResourceModel App\Models\User
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     */
    public function destroy(User $user)
    {
        $user->delete();
        return new UserResource($user);
    }
}
