<?php

namespace App\Http\Controllers;

use App\Exceptions\RoleHasUsersException;
use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Http\Resources\RoleResource;
use App\Models\Role;
use App\Models\User;
use App\Traits\BaseResponse;
use Illuminate\Http\Request;

/**
 * @group Role
 */
class RoleController extends Controller
{
    use BaseResponse;

    public function __construct()
    {
        $this->middleware(['can:roles.read'])->only(['index','show']);
        $this->middleware(['can:roles.create'])->only(['store']);
        $this->middleware(['can:roles.update'])->only(['update']);
        $this->middleware(['can:roles.delete'])->only(['destroy']);
    }
    /**
     * Display a listing of the roles.
     *
     * @apiResourceCollection App\Http\Resources\RoleResource
     * @apiResourceModel App\Models\Role
     * @responseFile 401 /responses/auth/unauthenticated.json
     */
    public function index()
    {
        return $this->successResponse(RoleResource::collection(Role::all()));
    }

    /**
     * Store a newly created role in storage.
     *
     * @apiResource App\Http\Resources\RoleResource
     * @apiResourceModel App\Models\Role
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 422 scenario="unprocessable content" /responses/role/form_validation_fails.json
     */
    public function store(CreateRoleRequest $request)
    {
        $role = Role::create(['name'=>$request->name])->syncPermissions($request->permissions);
        return $this->successResponse(new RoleResource($role));
    }

    /**
     * Display the specified role.
     *
     * @apiResource App\Http\Resources\RoleResource
     * @apiResourceModel App\Models\Role with=permissions
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     */
    public function show(Role $role)
    {
        return $this->successResponse(new RoleResource($role->load('permissions')));
    }

    /**
     * Update the specified role in storage.
     *
     * @apiResource App\Http\Resources\RoleResource
     * @apiResourceModel App\Models\Role
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     * @responseFile 422 scenario="unprocessable content" /responses/role/form_validation_fails.json
     */
    public function update(UpdateRoleRequest $request,Role $role)
    {
        $role->update($request->only('name'));
        if (isset($request->permissions)) $role->syncPermissions($request->permissions);
        return $this->successResponse(new RoleResource($role->refresh()->load('permissions')));
    }

    /**
     * Remove the specified role from storage.
     *
     * @apiResource App\Http\Resources\RoleResource
     * @apiResourceModel App\Models\Role
     * @apiResourceAdditional status=true
     * @responseFile 401 /responses/auth/unauthenticated.json
     * @responseFile 404 /responses/model.not_found.json
     */
    public function destroy(Role $role)
    {   
        $users = User::whereHas('roles',function($query) use($role) { $query->where('role_id',$role->id); })->count();
        if($users > 0 ) throw new RoleHasUsersException();
        $role->delete();
        return $this->successResponse(new RoleResource($role));
    }
}
