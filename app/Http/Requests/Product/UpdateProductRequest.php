<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => [
                'string','min:3',
                Rule::unique('products','name')->where(function($query){
                    $query->whereNull('deleted_at')
                        ->where('id','!=',$this->product->id);
                })
            ],
            'stock' => 'integer|min:0',
            'price' => 'integer',
            'minimum_stock' => 'integer|min:0'
        ];
    }
}
