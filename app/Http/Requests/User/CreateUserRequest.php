<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where(function($query){
                    $query->whereNull('deleted_at');
                })
            ],
            'password' => [
                Password::min(8)
                        ->mixedCase()
                        ->numbers()
                        ->symbols()
            ],
            'roles' => 'required|array|min:1',
            'roles.*' => 'required|integer|exists:roles,id'
        ];
    }
}
