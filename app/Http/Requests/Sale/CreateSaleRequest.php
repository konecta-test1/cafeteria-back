<?php

namespace App\Http\Requests\Sale;

use App\Rules\StockRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateSaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_id' => [
                'required',
                Rule::exists('users','id')->where(function($query){
                    $query->whereNull('deleted_at');
                })
            ],
            'products' => [
                'required',
                'array',
                'min:1',
            ],
            'products.*' => 'required|array:product_id,quantity',
            'products.*.product_id' => [
                'bail',
                'required',
                Rule::exists('products','id')->whereNull('deleted_at'),
                new StockRule($this->products)
            ],
            'products.*.quantity' => 'required|integer|min:1'
        ];
    }
}
