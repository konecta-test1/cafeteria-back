<?php

namespace App\Exceptions;

use App\Traits\BaseResponse;
use Exception;

class AuthServiceUnavialable extends Exception
{
    use BaseResponse;

    public function render()
    {
        return $this->errorResponse(['message'=>'Auth Service unavailable'],503);
    }
}
