<?php

namespace App\Exceptions;

use App\Traits\BaseResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use BaseResponse;
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (NotFoundHttpException  $e, $request) {
            
            if ($request->is('api/*')) {
                Log::error($e->getMessage());
                return $this->errorResponse([
                    'message' => $e->getMessage(),
                    'error' => "Not Found"
                ],404);
            }
        });

        $this->renderable(function (ValidationException  $e, $request) {
            
            if ($request->is('api/*')) {
                Log::error($e->getMessage());
                return $this->errorResponse([
                    'message' => $e->getMessage(),
                    'errors' => $e->validator->errors()
                ],422);
            }
        });

        $this->renderable(function (\Illuminate\Auth\AuthenticationException  $e, $request) {
            
            if ($request->is('api/*')) {
                Log::error($e->getMessage());
                return $this->errorResponse([
                    'message' => $e->getMessage()
                ],401);
            }
        });

        $this->renderable(function (AccessDeniedHttpException $e, $request) {
            if ($request->is('api/*')) {
                Log::error($e->getMessage());
                return $this->errorResponse([
                    'message' => 'You do not have the required authorization',
                ],403);
            }
        });
    }
}
