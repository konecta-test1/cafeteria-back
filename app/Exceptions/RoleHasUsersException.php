<?php

namespace App\Exceptions;

use App\Traits\BaseResponse;
use Exception;

class RoleHasUsersException extends Exception
{
    use BaseResponse;

    public function render()
    {
        return $this->errorResponse(['message'=>'This role is attached to some users'],422);
    }
}
