<?php

namespace App\Observers;

use App\Models\Product;
use App\Models\User;
use App\Notifications\MinimumStockReached;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class ProductObserver
{

    /**
     * Handle the Product "saving" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function saving(Product $product)
    {
        if($product->isDirty('stock')){
            // stock has changed
            if ($product->stock <= $product->minimum_stock) {
                $users = User::whereHas('roles', function($query){
                    $query->whereIn('id',[1,2]);
                })->get();
                Notification::send($users,new MinimumStockReached($product));
            }
        }
    }
}
